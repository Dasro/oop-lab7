#ifndef NOTBLAD_H
#define NOTBLAD_H

#include <vector>
#include <not.h>
#include <QPixmap>

#define MAX_NR_OF_NOTES 14

    class Notblad
    {
    private:
        std::vector<Not*> _noter;

    public:
        Notblad();
        void addNot(Not &note);
        void deleteNote();
        std::vector<Not*> getNotes();
    };

#endif // NOTBLAD_H
