#include "notblad.h"
#include "QDebug"

Notblad::Notblad()
{

}

void Notblad::addNot(Not &note){

    if(_noter.size()<MAX_NR_OF_NOTES){

        note.setOrderNr(_noter.size());
        _noter.push_back(new Not(note));
    }

}

void Notblad::deleteNote(){

    if(_noter.size()!=NULL)
        _noter.pop_back();                                      // SE OM VECTOR UTFÖR DELETE VID POP
}

std::vector<Not*> Notblad::getNotes(){
    return _noter;
}
