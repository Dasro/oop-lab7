#include "not.h"

using namespace std;

Not::Not(){

    _lenght = 0;
    _height = 0;
    _png = new QPixmap;
    _png = nullptr;
    _pngC = new QPixmap;
    _pngC = nullptr;
}

Not::Not(int orderNr, int lenght, int height, const QPixmap *png, QPixmap *pngC){

    _lenght = lenght;
    _height = height;
    _png = new QPixmap(*png);
    _pngC = nullptr;
}

Not::Not(Not &other){

    _lenght = other.getLenght();
    _height = other.getHeight();
    _orderNr = other.getOrderNr();
    _png = new QPixmap(*other.getPng());
    _pngC = new QPixmap(*other.getPngC());
}

Not::~Not(){

    delete _png;
    delete _pngC;
}

void Not::drawNote(QPainter *painter){

    if(_height == NULL && _pngC != nullptr)
        painter->drawPixmap(90+(_orderNr*60), 386-(_height*8), *_pngC);
    else painter->drawPixmap(90+(_orderNr*60), 386-(_height*8), *_png);
}

void Not::play(Synthesizer &synthesizer){

    synthesizer.spela(_height, _lenght, 0);
}

int Not::getLenght(){

    return _lenght;
}

void Not::setLength(int newLenght){

    _lenght = newLenght;
}

int Not::getHeight(){

    return _height;
}

void Not::setHeight(int newHeight){

    _height = newHeight;
}

int Not::getOrderNr(){

    return _orderNr;
}

void Not::setOrderNr(int newOrderNr){

    _orderNr = newOrderNr;
}

const QPixmap* Not::getPng(){

    return _png;
}

void Not::setPng(const QPixmap *newPng){

    _png = newPng;
}

QPixmap* Not::getPngC(){

    return _pngC;
}

void Not::setPngC(QPixmap *newPngC){

    _pngC = newPngC;
}
