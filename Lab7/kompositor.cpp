#include "kompositor.h"
#include "ui_kompositor.h"
#include "QDebug"
#include "QMouseEvent"
#include "QPainter"

Kompositor::Kompositor(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::Kompositor)
{
    /* UI */
    _ui->setupUi(this);

    /* DELETE BUTTON SIGNAL */
    connect(_ui->button_deleteNote, SIGNAL(clicked(bool)), this, SLOT(deleteButtonClicked()));

    /* PLAY BUTTON SIGNAL */
    connect(_ui->button_play, SIGNAL(clicked(bool)), this, SLOT(playButtonClicked()));

    /* NOTE BUTTON SIGNALS */
    connect(_ui->button_eight, SIGNAL(toggled(bool)), this, SLOT(eightClicked()));
    connect(_ui->button_eightrest, SIGNAL(toggled(bool)), this, SLOT(eightRestClicked()));
    connect(_ui->button_half, SIGNAL(toggled(bool)), this, SLOT(halfClicked()));
    connect(_ui->button_quater, SIGNAL(toggled(bool)), this, SLOT(quaterClicked()));
    connect(_ui->button_quaterrest, SIGNAL(toggled(bool)), this, SLOT(quaterRestClicked()));

    /* ATTRIBUTE ALLOCATION AND ASSIGNMENT */
    _eightC = new QPixmap(":/Images/eightc.png");
    _halfC = new QPixmap(":/Images/halfc.png");
    _quaterC = new QPixmap(":/Images/quaterc.png");
    _ui->button_half->setChecked(false);
    _noteIsChecked = false;
}

Kompositor::~Kompositor(){

    delete _ui;
    delete _timer;
    delete _eightC;
    delete _halfC;
    delete _quaterC;
}

void Kompositor::update(){

    repaint();
}

void Kompositor::deleteButtonClicked(){

    _notBlad.deleteNote();
}

void Kompositor::playButtonClicked(){

    for(int i=0; i<_notBlad.getNotes().size(); i++)
        _notBlad.getNotes()[i]->play(_synthesizer);
}

void Kompositor::eightClicked(){

    _noteType.setPng(_ui->not_eight->pixmap());
    _noteType.setPngC(_eightC);
    _noteType.setLength(250);
    _noteIsChecked = true;
}

void Kompositor::eightRestClicked(){

    _noteType.setPng(_ui->not_eightrest->pixmap());
    _noteType.setLength(250);
    _noteIsChecked = true;
}

void Kompositor::halfClicked(){

    _noteType.setPng(_ui->not_half->pixmap());
    _noteType.setPngC(_halfC);
    _noteType.setLength(1000);
    _noteIsChecked = true;
}

void Kompositor::quaterClicked(){

    _noteType.setPng(_ui->not_quater->pixmap());
    _noteType.setPngC(_quaterC);
    _noteType.setLength(500);
    _noteIsChecked = true;
}

void Kompositor::quaterRestClicked(){

    _noteType.setPng(_ui->not_quaterrest->pixmap());
    _noteType.setLength(500);
    _noteIsChecked = true;
}

void Kompositor::mousePressEvent(QMouseEvent *event){

    _mouseX = event->x();
    _mouseY = event->y();

    if(_noteIsChecked)
        if(_mouseY>356 && _mouseY<=446){
            _noteType.setHeight(pixelToHeight(_mouseY));
            _notBlad.addNot(_noteType);
            update();
        }
}

void Kompositor::paintEvent(QPaintEvent *event){

    QPainter painter(this);

    for(int i=0; i<_notBlad.getNotes().size(); i++)
        _notBlad.getNotes()[i]->drawNote(&painter);
}

int Kompositor::pixelToHeight(int yPixel){

    float eLevel = 426;
    float step = 8;
    int height = -1;

    for(int i = 0; i < 12; i++){

        if(yPixel <= (eLevel + (2-i)*step + step/2) && yPixel >
                (eLevel + (2-i)*step - step/2))
            height = i;
    }

    return height;
}


