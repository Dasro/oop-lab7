#-------------------------------------------------
#
# Project created by QtCreator 2018-05-15T16:47:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab7
TEMPLATE = app

LIBS += -lwinmm
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += __WINDOWS_MM__

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        kompositor.cpp \
    notblad.cpp \
    not.cpp \
    RtMidi.cpp \
    Synthesizer.cpp

HEADERS += \
        kompositor.h \
    notblad.h \
    not.h \
    RtMidi.h \
    Synthesizer.h

FORMS += \
        kompositor.ui

RESOURCES = \
    resources.qrc
