#ifndef KOMPOSITOR_H
#define KOMPOSITOR_H

#include <QMainWindow>
#include <QLabel>
#include <QTimer>
#include <notblad.h>
#include <Synthesizer.h>
//#include <RtMidi.h>
#include <not.h>
#include <QString>
#include <QPixmap>

namespace Ui {
class Kompositor;
}

class Kompositor : public QMainWindow
{
    Q_OBJECT

private:
    Ui::Kompositor *_ui;
    Not _noteType;
    QTimer *_timer;
    Synthesizer _synthesizer;
    Notblad _notBlad;
    QPixmap *_eightC;
    QPixmap *_halfC;
    QPixmap *_quaterC;
    int _mouseX;
    int _mouseY;
    bool _noteIsChecked;

private slots:
    void deleteButtonClicked();
    void playButtonClicked();
    void eightClicked();
    void eightRestClicked();
    void halfClicked();
    void quaterClicked();
    void quaterRestClicked();

public:
    explicit Kompositor(QWidget *parent = 0);
    ~Kompositor();
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    int pixelToHeight(int yPixel);
    void update();
};

#endif // KOMPOSITOR_H
