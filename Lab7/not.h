#ifndef NOT_H
#define NOT_H

#include <QPixmap>
#include <QPainter>
#include <Synthesizer.h>
#include <RtMidi.h>

class Not
{
private:
    int _lenght;
    int _height;
    int _orderNr;
    const QPixmap *_png;
    QPixmap *_pngC;

public:
    Not();
    Not(int orderNr, int lenght, int hight, const QPixmap *png, QPixmap *pngC);
    Not(Not &other);
    ~Not();
    void drawNote(QPainter *painter);
    void play(Synthesizer &synthesizer);
    int getLenght();
    void setLength(int newLenght);
    int getHeight();
    void setHeight(int newHeight);
    int getOrderNr();
    void setOrderNr(int newOrderNr);
    const QPixmap* getPng();
    void setPng(const QPixmap *newPng);
    QPixmap* getPngC();
    void setPngC(QPixmap *newPngC);


};

#endif // NOT_H
